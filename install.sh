conda create -n morpher-env python=3.6 anaconda

conda deactivate
conda activate morpher-env

conda install -c conda-forge matplotlib
conda install -c conda-forge -c dlr-sc -c pythonocc -c oce pythonocc-core==7.4.0 python=3.6
conda install -c conda-forge numpy-stl
conda install -c conda-forge scipy
conda install -c conda-forge black

