# cad-morpher

PyGem based tool to morph CAD geometries using reference STL geometries.

## Installation ##
* Install anaconda [https://www.anaconda.com/products/individual]
* Follow _install.sh_

## Documentation ##
See _Documentation/code.md_ and _Documentation/dev.md_

## Usage and Examples ##
See _usage_and_examples.md_