# Usage and Examples #
Run execute the examples in this directory. Run `conda activate morpher-env` to setup the environment.

## Different Modes ##

For parameter names and more information, run 

`python3 -s ../morpher.py --help` 

### POINTS ###
Output control points and normals

![](/Documentation/media/points.svg)

### SHOW ###

![](/Documentation/media/show1.svg)
![](/Documentation/media/show2.svg)

### DEVIATION ###

![](/Documentation/media/deviation.svg)

### COMPENSATE ###

![](/Documentation/media/compensation.svg)

### MORPH ###

![](/Documentation/media/morph.svg)

## Visualizing a geometry with control points and normals ##
`python3 -s ../morpher.py POINTS -i geometries/box.igs -co points-out -no normals-out`

`python3 -s ../morpher.py SHOW -i geometries/box.igs -ci points-out.npy -ni normals-out.npy`

## Manipulating control points with some other script ##
`python3 -s ../morpher.py POINTS -i geometries/box.igs -co points-out -no normals-out`

`./some-other-script points-out.npy`

`python3 -s ../morpher.py MORPH -i geometries/box.igs -ci points-out.npy -ni normals-out.npy -o result.igs`

## Calculating deviation of some points on the geometry ##

`python3 -s ../morpher.py DEVIATION -i geometries/box.igs -r geometries/box_mesh_def.stl -c closest-point -ri points.npy -do deviation`

## Using closest-point compensation ##

`python3 -s ../morpher.py COMPENSATION -i geometries/box.igs -r geometries/box_mesh_def.stl -c closest-point -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/rail.igs -r geometries/rail_mesh_def.stl -c closest-point -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/sphere.igs -r geometries/sphere_mesh_def.stl -c closest-point -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/part.igs -r geometries/part_mesh_def.stl -c closest-point -o result.igs`

## Using reverse-normals compensation ##

`python3 -s ../morpher.py COMPENSATION -i geometries/box.igs -r geometries/box_mesh_def.stl -c reverse-normals -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/rail.igs -r geometries/rail_mesh_def.stl -c reverse-normals -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/sphere.igs -r geometries/sphere_mesh_def.stl -c reverse-normals -o result.igs`

`python3 -s ../morpher.py COMPENSATION -i geometries/part.igs -r geometries/part_mesh_def.stl -c reverse-normals -o result.igs`


