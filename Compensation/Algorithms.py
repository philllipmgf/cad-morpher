def calculate_closest_point_deviation(mesh, control_points):
    deviation = control_points.copy()
    for i in range(0, control_points.shape[0]):
        print(
            "calculating deviation for control point {} out of {}".format(
                i + 1, control_points.shape[0]
            )
        )

        # find the closest point on the reference mesh mesh
        x, y, z = mesh.find_closest_triangle_intersection(control_points[i])

        # the deviation is the simple per-axis distance
        dx = control_points[i][0] - x
        dy = control_points[i][1] - y
        dz = control_points[i][2] - z

        # move the control point in the other direction
        deviation[i][0] = -dx
        deviation[i][1] = -dy
        deviation[i][2] = -dz

    return deviation


def calculate_reverse_normal_deviation(mesh, control_points, normals):
    deviation = control_points.copy()
    for i in range(0, control_points.shape[0]):
        print(
            "calculating deviation for control point {} out of {}".format(
                i + 1, control_points.shape[0]
            )
        )

        # iterate all triangles and find one along the normal vector
        # if there are two, choose the closest
        t = mesh.find_closest_intersection(control_points[i], normals[i])

        # if no triangle was found, do nothing
        if t == float("inf"):
            t = 0

        # the compensation is along the normal in the opposite direction from the intersection
        deviation[i][0] = normals[i][0] * -t
        deviation[i][1] = normals[i][1] * -t
        deviation[i][2] = normals[i][2] * -t

    return deviation


def apply_fix(control_points, deviation):
    for i in range(0, control_points.shape[0]):
        control_points[i][0] = control_points[i][0] + deviation[i][0]
        control_points[i][1] = control_points[i][1] + deviation[i][1]
        control_points[i][2] = control_points[i][2] + deviation[i][2]
