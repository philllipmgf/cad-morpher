# Existing Code #

The current implementation consists of wrapper code for _PyGem_, some STL handling for the reference files and 
a command line interface.

## PyGemWrapper ##

Wraps _PyGem_ code that is used for the morphing. _PyGem_ is calling _PythonOCC_ functions. 

### FileHandler.py ###
Abstract interface used by _IgesHandler.py_ and _NurbsHandler.py_

### IgesHandler.py ###
Extends _NurbsHandler_ and handles reading and writing files in _IGES_ format. 

#### load_shape_from_file(self, filename): ####
Reads an IGES file from disk. Returns a _shape_ object, an _OpenCascade_ geometry object.

#### write_shape_to_file(self, filename): ####
Writes a shape object to an IGES file.

### NurbsHandler.py ###
This is where heavy lifting is done.

#### parse(self, filename): ####
This method reads an input file, producing the control points as a numpy array. We have extended it with _get_normal_for_point_ 
that produces the normal vectors for the control points, wherever possible. See inline comments in _NurbsHandler.py_.

#### write(self, mesh_points, filename, tolerance=None): ####
This methods receives an array of modified control points for the parsed geometry, reconstructs the geometry and writes
the result to file. A major change compared to the original code is handling of trimmed surfaces with holes. 
See inline comments in _NurbsHandler.py_ and _future_work_ for more details.

#### get_normal_for_point(face, u, v): ####
This methods attempts to get a normal for a point by checking neighboring points in the _u,v_ plane.

#### show(self, show_file=None, points=None, directions=None): ####
Visualizes the geometry with an interactive viewer. Supplying both _points_ and _directions_ as numpy arrays 
allows visualization of the control points and a direction vector. We use this to visualize the compensation
to be done on the original geometry.

## Compensation ##
### Algorithms.py ###
#### perform_closest_point_compensation(mesh, control_points): ####
Performs closest point compensation as follows:

1. For each control point, find the closest triangle using the minimal euclidean distance to the triangle's centroid
2. For this triangle, use an orthogonal projection to find the closest point _p_ on the plane defined by the triangle (it 
is not guaranteed be on the actual triangle)
3. Let _d_, be the distance from the control point to the closest point _p_, 
4. New control point location is calculated by moving it in the opposite direction from _p_ by the distance _d_

Returns array of the new control points and the deviation vectors.

#### perform_reverse_normals_compensation(mesh, control_points, normals): ####
Performs closest point compensation as follows:

1. For each control point, find a triangle that intersects the ray along the normal (in both directions)
2. As there could be two or more such triangles, choose the closest and save the distance to it as _d_
3. If not found, leave the control point unchanged
4. New control point location is calculated by moving it _d_ units along the normal, in the reverse direction to the
intersection point found in _1._ 

Returns an array of the new control points and the deviation vectors.

## Stl ##
### StlHandler.py ###
Uses _[numpy-stl](https://numpy-stl.readthedocs.io/en/latest/)_ package to handle triangular meshes in STL file format. 

#### load(self, filename):####
Loads the triangular mesh object from a file. It is saved in the mesh property. 

#### orthogonal_projection_to_triangle(point, triangle): ####
Calculates the orthogonal projection of a point onto a triangle. This is the closest point on the plane defined
by the triangle. See inline comments for more details.

#### find_closest_triangle(self, point): ####
Iterates all triangles in the mesh and returns the triangle for which the center has the smallest euclidean
distance to the input point.  See inline comments for more details.

### RayVector.py ###
Ray - Vector intersection detection algorithm. Used for reverse normal compensation.
Source: https://github.com/johnnovak/raytriangle-test/