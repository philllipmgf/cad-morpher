# Future Work #

As an experimental project, many open questions and improvements remain. In this part we will try to explain current
 open issues and suggest a few enhancements.
 
## Compensation and control points ##

_PyGem_ is designed for free-form and other geometry manipulation techniques. They all abstract away the control points
and let a user easily deform a 3D shape. This is useful when stretching, squeezing and deforming a shape as whole with user input. 
They all work because moving around multiple NURBS control points simultaneously deforms a shape nicely and in an 
expected way. 

In this project, we try to deform a shape based on deviation from another measured geometry. We measure the deviation
at the control points and use this information to move them. Unfortunately, not all control points are actually on the surface. 
In some cases, the NURBS control points are far outside the geometry. To see an example, compare `box.igs` and `sphere.igs`
and consider the control points.


### Possible solutions ###
#### Reparameterization ####
Given a simple original geometry, it is possible that the reference geometry is more complicated and the morphed shape
needs more control points and details. As a first step, it is probably necessary to reparameterize geometries to have
more control points. Possibly, this can be done using boolean operations without explicitly handling the surface objects.

#### Operating on knots ####
B-Spline curve and surfaces use both control points and knots. Knots are per definition on the curve / surface and define
the points on which different sections of the spline meet. 

![](Documentation/media/1280px-Parametic_Cubic_Spline.svg.png "Spline with knots")
https://en.wikipedia.org/wiki/Spline_(mathematics)#/media/File:Parametic_Cubic_Spline.svg

Each knot vector defines the "influence" of control points on this location, depending on the degree. As the knots are 
on the curve / surface, it is more reasonable to calculate their new compensated positions rather than the control points.
We cannot control the knots' positions directly, however as they represent a weighted sum of a few control points, it is
simple enough to "project" the required transformation on the control points, weighted by the knot vector.

1. Initiate a translation vector for each control point (0,0,0)
2. For each knot in the surface or curve:
    
    2.1. Calculate deviation to the reference geometry and a compensation translation
    2.2. Update the translation vector for each relevant control point, weighted by the knot vector
    
3. Apply the translations to each control points

See [paper](https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/B-spline/bspline-mod-knot.html)

## Deviation and compensation ##
Two very simplistic deviation measurement approaches were implemented, using the normals at the control points and 
finding the closest point on the reference geometry. Using normals and triangle intersections seems to fail for many 
points, possibly due to meshing issues or problems with the intersection algorithm. Performing compensation along normals
only allows for movement in the normal direction, possibly causing parts to drift away from each other.

Measuring deviation to the closest mesh point ensures that close points on different surfaces will be compensated in the 
same way. However there is no notion of correspondence nor "feasibility check". A part on the reference geometry can be
very close to the original geometry, "pulling" many reference points towards or away from a single point. A more complete
solution must include some kind of correspondence, matching each reference point to a plausible point on the reference
geometry.

For a nice review of different shape correspondence algorithms, see [paper](https://www.cs.princeton.edu/courses/archive/spring11/cos598A/pdfs/vanKaick10.pdf) 


## Read and produce STEP files ## 
STEP is a more useful and common geometry format then IGES. It seems like _PyGem_ supports reading and writing STEP files,
however no examples were available while writing this document.


