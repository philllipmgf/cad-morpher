# Development Guide #

## Requirements and Installation ##
[anaconda](https://www.anaconda.com/products/individual) is the package manager used for this project. After installing it,
use the script `install.sh` to setup the development environment. `conda activate morpher-env` should be executed to start 
the virtual environment. It is not recommended to build _pythonocc_ from source, as API changes would likely break existing code.

## PyGem ##

The code is largely based on [PyGem](https://github.com/mathLab/PyGeM), a CAD morphing toolkit. As some changes and bug
fixes were needed, used code was copied into _PyGemWrapper_ and modified there. In this project, we use _PyGem_ for reading
CAD files, exporting the control points and reconstructing geometries. Known issues and limitations are described in 
the _future work_ document.

## OpenCascade / PythonOCC ##

Our code and _PyGem_ use _PythonOCC_ as a wrapper for [OpenCascade](https://www.opencascade.com/). It is an open source 
cad modelling environment. _PythonOCC_ is a partly automatically generated wrapper for a C++ library. The interface is not _pythonic_
and is generally not very clear. Documentation is extremely lacking.

### Basic Modelling Concepts ###

In general, 3D models are modelled as a combination of topology and geometry.
A 3D cube with 6 faces consists of the following topological entities - 6 faces, 12 edges, 8 vertices. Each face consists
of 4 edges. Each edge connects two vertices.

The topology only defines connectivity of different entities in an hierarchical way. Different geometries are attached to
each topological object. A vertex is assigned a point, a 3D vector. An edge is attached to a 3D curve. A 3D surface is 
attached to a face. 

For more complicated scenarios, additional topology and geometry can be attached. For example a face with a round hole
in its center has an additional edge around it, with a _BCurve_, parameterized on the face surface geometry.

PyGem uses BSpline geometries. More information can be found in [Wikipedia](https://en.wikipedia.org/wiki/B-spline).

_BRep_ representation is a common way to represent 3D objects implicitly by defining a closed surface, whereas the inside
is "solid". _PyGem_ 

### Basic Modelling with _PythonOCC_ ###

A 3D model is represented in a _TopoDS_Shape_ object. Topological entities have a type that starts with _topods_, in the package
_OCC.Core.TopoDS_. For example: _topods_Face_, _topods_Edge_. 

Geometrical objects used in the code are points _gp_Pnt_, general surface and curve entities.

Iterating arrays from _PythonOCC_ is done in a non-pythonic, _Iterator_ style. Iterating faces in a shape for example:

````
faces_explorer = TopExp_Explorer(self.shape, TopAbs_FACE)
while faces_explorer.More():
    face = topods_Face(faces_explorer.Current())
    ...
    faces_explorer.Next()
````

Performing an operation with an array is done by calling the _Add()_ method. For example, Constructing a wire from multiple edges:
````
wire_maker = BRepBuilderAPI_MakeWire()
edge_explorer = TopExp_Explorer(nurbs_face, TopAbs_EDGE)
while edge_explorer.More():
    edge = topods_Edge(edge_explorer.Current())
    ...
    wire_maker.Add(edge)
    edge_explorer.Next()

wire = wire_maker.Wire() # Create the wire from all edges
````

Reading different properties of entities is done by single use "Analysis" objects. For example:
````
# Getting a face normal in point (u, v)
GeomLProp_SLProps(face, u, v, 1, 0.01).Normal()

# Getting the u, v parameters in a specific position
sa = ShapeAnalysis_Surface(brep_face)
uv = sa.ValueOfUV(control_point_coordinates, 0.0001)
u = uv.X()
v = uv.Y()
````

In general, the interface is not consistent or pythonic. It's recommended to check the examples (below) if possible.

### Resources for OpenCascade / PythonOCC ###
* https://github.com/tpaviot/pythonocc-demos 
* [An introductory guide to _PytonOCC_](http://trac.lecad.si/vaje/raw-attachment/wiki/PythonOcc/VisualizationOfGeometryWithUtilisingpythonOCC.pdf)
* Some questions were answered in [StackOverflow](https://stackoverflow.com/)
* OpenCascade [Forum](https://www.opencascade.com/forums) has some useful discussions, mostly for the C++ interface
* Official documentation for _OpenCascade_ is available [here](https://www.opencascade.com/content/documentation)
