import math

import numpy as np

from stl import mesh
from .RayVector import Vec3
from .RayVector import Ray
from .RayVector import ray_triangle_intersect


class StlHandler(object):
    def load(self, filename):
        self.mesh = mesh.Mesh.from_file(filename)
        print(self.find_mins_maxs())

    @staticmethod
    def orthogonal_projection_to_triangle(point, triangle):
        point_x = point[0]
        point_y = point[1]
        point_z = point[2]

        # find the plane through three points
        # http://kitchingroup.cheme.cmu.edu/blog/2015/01/18/Equation-of-a-plane-through-three-points/
        p1 = triangle[0]
        p2 = triangle[1]
        p3 = triangle[2]

        # These two vectors are in the plane
        v1 = p3 - p1
        v2 = p2 - p1

        # the cross product is a vector normal to the plane
        cp = np.cross(v1, v2)
        a, b, c = cp

        norm = math.sqrt(math.pow(a, 2) + math.pow(b, 2) + math.pow(c, 2))
        a /= norm
        b /= norm
        c /= norm

        # find orthogonal projection on the plane
        # https://math.stackexchange.com/questions/100761/how-do-i-find-the-projection-of-a-point-onto-a-plane
        d, e, f = p1[0], p1[1], p1[2]

        t = (
            (a * d) - (a * point_x) + (b * e) - (b * point_y) + (c * f) - (c * point_z)
        ) / (math.pow(a, 2) + math.pow(b, 2) + math.pow(c, 2))
        x = point_x + t * a
        y = point_y + t * b
        z = point_z + t * c

        return x, y, z

    def find_closest_triangle(self, point):
        closest_triangle = None
        min_distance = float("inf")

        point_x = point[0]
        point_y = point[1]
        point_z = point[2]

        for triangle in self.mesh.data["vectors"]:
            triangle_center_x = (triangle[0][0] + triangle[1][0] + triangle[2][0]) / 3
            triangle_center_y = (triangle[0][1] + triangle[1][1] + triangle[2][1]) / 3
            triangle_center_z = (triangle[0][2] + triangle[1][2] + triangle[2][2]) / 3
            dx = triangle_center_x - point_x
            dy = triangle_center_y - point_y
            dz = triangle_center_z - point_z
            distance_to_triangle = math.sqrt(
                math.pow(dx, 2) + math.pow(dy, 2) + math.pow(dz, 2)
            )
            if distance_to_triangle < min_distance:
                min_distance = distance_to_triangle
                closest_triangle = triangle

        return closest_triangle

    def find_closest_triangle_intersection(self, point):
        # find closest triangle and the closest point on it using orthogonal projection
        closest_triangle = self.find_closest_triangle(point)
        x, y, z = StlHandler.orthogonal_projection_to_triangle(point, closest_triangle)

        return x, y, z

    def find_closest_intersection(self, point, normal):
        min = float("inf")
        plus_ray = Ray(
            Vec3(point[0], point[1], point[2]), Vec3(normal[0], normal[1], normal[2])
        )
        minus_ray = Ray(
            Vec3(point[0], point[1], point[2]), Vec3(-normal[0], -normal[1], -normal[2])
        )

        for triangle in self.mesh.data["vectors"]:
            v1 = Vec3(triangle[0][0], triangle[0][1], triangle[0][2])
            v2 = Vec3(triangle[1][0], triangle[1][1], triangle[1][2])
            v3 = Vec3(triangle[2][0], triangle[2][1], triangle[2][2])
            t = ray_triangle_intersect(plus_ray, v1, v2, v3)
            if t != float("-inf"):
                if abs(t) < abs(min):
                    min = t

            t = ray_triangle_intersect(minus_ray, v1, v2, v3)
            if t != float("-inf"):
                if abs(t) < abs(min):
                    min = t

        return min

    def find_mins_maxs(self):
        minx = maxx = miny = maxy = minz = maxz = None
        for triangle in self.mesh.data["vectors"]:
            for p in triangle:
                if minx is None:
                    minx = p[0]
                    maxx = p[0]
                    miny = p[1]
                    maxy = p[1]
                    minz = p[2]
                    maxz = p[2]
                else:
                    maxx = max(p[0], maxx)
                    minx = min(p[0], minx)
                    maxy = max(p[1], maxy)
                    miny = min(p[1], miny)
                    maxz = max(p[2], maxz)
                    minz = min(p[2], minz)
        return minx, maxx, miny, maxy, minz, maxz
