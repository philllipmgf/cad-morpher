"""
Derived module from filehandler.py to handle iges/igs and step/stp files.
Implements all methods for parsing an object and applying FFD.
File handling operations (reading/writing) must be implemented
in derived classes.
"""
import os
import math
import numpy as np
from OCC.Core.BRep import BRep_Tool, BRep_Builder, BRep_Tool_Curve
from OCC.Core.BRepBuilderAPI import (
    BRepBuilderAPI_MakeEdge,
    BRepBuilderAPI_MakeFace,
    BRepBuilderAPI_NurbsConvert,
    BRepBuilderAPI_MakeWire,
    BRepBuilderAPI_Sewing,
)
from OCC.Display.SimpleGui import init_display
from OCC.Core.GeomConvert import (
    geomconvert_SurfaceToBSplineSurface,
    geomconvert_CurveToBSplineCurve,
)
from OCC.Core.gp import gp_Pnt, gp_XYZ
from OCC.Core.ShapeFix import ShapeFix_ShapeTolerance, ShapeFix_Shell
from OCC.Core.StlAPI import StlAPI_Writer
from OCC.Core.TopAbs import (
    TopAbs_FACE,
    TopAbs_EDGE,
    TopAbs_WIRE,
    TopAbs_FORWARD,
    TopAbs_SHELL,
)
from OCC.Core.TopExp import TopExp_Explorer, topexp
from OCC.Core.TopoDS import (
    topods_Face,
    TopoDS_Compound,
    topods_Shell,
    topods_Edge,
    topods_Wire,
    topods,
    TopoDS_Shape,
)
from OCC.Core.ShapeAnalysis import ShapeAnalysis_Surface
from OCC.Core.GeomLProp import GeomLProp_SLProps
from OCC.Extend.ShapeFactory import make_vertex

from OCC.Extend.DataExchange import read_stl_file

import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from stl import mesh


from .FileHandler import FileHandler


class NurbsHandler(FileHandler):
    """
    Nurbs file handler base class

    :cvar str infile: name of the input file to be processed.
    :cvar str outfile: name of the output file where to write in.
    :cvar list control_point_position: index of the first NURBS
        control point (or pole) of each face of the files.
    :cvar TopoDS_Shape shape: shape meant for modification.
    :cvar float tolerance: tolerance for the construction of the faces
        and wires in the write function. Default value is 1e-6.

    .. warning::

        For non trivial geometries it could be necessary to increase the
        tolerance. Linking edges into a single wire and then trimming the
        surface with the wire can be hard for the software, especially when the
        starting CAD has not been made for analysis but for design purposes.
    """

    def __init__(self):
        super(NurbsHandler, self).__init__()
        self._control_point_position = None
        self.tolerance = 1e-6
        self.shape = None
        self.check_topo = 0

    def _check_infile_instantiation(self):
        """
        This private method checks if `self.infile` and `self.shape` are
        instantiated. If not it means that nobody called the parse method
        and at least one of them is None. If the check fails it raises a
        RuntimeError.
        """
        if not self.shape or not self.infile:
            raise RuntimeError("You can not write a file without having parsed one.")

    def load_shape_from_file(self, filename):
        """
        Abstract method to load a specific file as a shape.

        Not implemented, it has to be implemented in subclasses.
        """
        raise NotImplementedError(
            "Subclass must implement abstract method"
            "{}.load_shape_from_file".format(self.__class__.__name__)
        )

    def parse(self, filename):
        """
        Method to parse the file `filename`. It returns a matrix with all
        the coordinates.

        :param string filename: name of the input file.

        :return: mesh_points: it is a `n_points`-by-3 matrix containing
            the coordinates of the points of the mesh
        :rtype: numpy.ndarray

        """
        self.infile = filename
        self.shape = self.load_shape_from_file(filename)

        # cycle on the faces to get the control points
        # init some quantities
        n_faces = 0
        control_point_position = [0]
        faces_explorer = TopExp_Explorer(self.shape, TopAbs_FACE)
        mesh_points = np.zeros(shape=(0, 3))
        normals = np.zeros(shape=(0, 3))

        # iterate all faces, convert to BSpline and then extract control points and normals
        while faces_explorer.More():
            # performing some conversions to get the right format (BSplineSurface)
            face = topods_Face(faces_explorer.Current())

            # convert geometries to NURBS geometries
            # curves are converted to nurbs curves, surfaces to nurbs surfaces
            nurbs_converter = BRepBuilderAPI_NurbsConvert(face)
            nurbs_converter.Perform(face)

            # convert the nurbs surface geometry to BSpline surface
            nurbs_face = nurbs_converter.Shape()
            brep_face = BRep_Tool.Surface(topods_Face(nurbs_face))
            bspline_face = geomconvert_SurfaceToBSplineSurface(brep_face)
            occ_face = bspline_face

            # shape analysis object
            sa = ShapeAnalysis_Surface(brep_face)

            # extract the Control Points of each face
            num_poles_u = occ_face.NbUPoles()
            num_poles_v = occ_face.NbVPoles()
            control_polygon_coordinates = np.zeros(shape=(num_poles_u * num_poles_v, 3))
            control_polygon_normals = np.zeros(shape=(num_poles_u * num_poles_v, 3))

            # cycle over the poles to get their coordinates
            i = 0
            for pole_u_direction in range(num_poles_u):
                for pole_v_direction in range(num_poles_v):
                    control_point_coordinates = occ_face.Pole(
                        pole_u_direction + 1, pole_v_direction + 1
                    )
                    control_polygon_coordinates[i, :] = [
                        control_point_coordinates.X(),
                        control_point_coordinates.Y(),
                        control_point_coordinates.Z(),
                    ]

                    # above we get the control point coordinates, get the u,v values as well
                    uv = sa.ValueOfUV(control_point_coordinates, 0.0001)
                    u = uv.X()
                    v = uv.Y()

                    # get the normal at each control point
                    normal = NurbsHandler.get_normal_for_point(brep_face, u, v)
                    control_polygon_normals[i, :] = [normal.X(), normal.Y(), normal.Z()]

                    i += 1

            # pushing the control points coordinates to the mesh_points array
            # (used for FFD)
            mesh_points = np.append(mesh_points, control_polygon_coordinates, axis=0)

            # pushing the control points normals to the normals array
            normals = np.append(normals, control_polygon_normals, axis=0)

            control_point_position.append(
                control_point_position[-1] + num_poles_u * num_poles_v
            )

            n_faces += 1
            faces_explorer.Next()
        self._control_point_position = control_point_position
        return mesh_points, normals

    def write(self, mesh_points, filename, tolerance=None):
        """
        Writes a output file, called `filename`, copying all the structures
        from self.filename but the coordinates. `mesh_points` is a matrix
        that contains the new coordinates to write in the output file.

        :param numpy.ndarray mesh_points: it is a *n_points*-by-3 matrix
            containing the coordinates of the points of the mesh.
        :param str filename: name of the output file.
        :param float tolerance: tolerance for the construction of the faces
            and wires in the write function. If not given it uses
            `self.tolerance`.
        """
        self._check_filename_type(filename)
        self._check_extension(filename)
        self._check_infile_instantiation()

        self.outfile = filename

        if tolerance is not None:
            self.tolerance = tolerance

        # cycle on the faces to update the control points position
        # init some quantities
        faces_explorer = TopExp_Explorer(self.shape, TopAbs_FACE)
        n_faces = 0
        control_point_position = self._control_point_position

        # initiate an empty shape
        compound_builder = BRep_Builder()
        compound = TopoDS_Compound()
        compound_builder.MakeCompound(compound)

        while faces_explorer.More():
            # similar to the parser method
            face = topods_Face(faces_explorer.Current())
            nurbs_converter = BRepBuilderAPI_NurbsConvert(face)
            nurbs_converter.Perform(face)
            nurbs_face = nurbs_converter.Shape()
            face_aux = topods_Face(nurbs_face)
            brep_face = BRep_Tool.Surface(topods_Face(nurbs_face))
            bspline_face = geomconvert_SurfaceToBSplineSurface(brep_face)
            # occ_face = bspline_face.GetObject()
            occ_face = bspline_face

            n_poles_u = occ_face.NbUPoles()
            n_poles_v = occ_face.NbVPoles()

            # create a face with the new control points
            i = 0
            for pole_u_direction in range(n_poles_u):
                for pole_v_direction in range(n_poles_v):
                    control_point_coordinates = mesh_points[
                        i + control_point_position[n_faces], :
                    ]
                    point_xyz = gp_XYZ(*control_point_coordinates)

                    gp_point = gp_Pnt(point_xyz)
                    occ_face.SetPole(
                        pole_u_direction + 1, pole_v_direction + 1, gp_point
                    )
                    i += 1

            # construct the deformed wire for the trimmed surfaces
            wire_maker = BRepBuilderAPI_MakeWire()
            tol = ShapeFix_ShapeTolerance()

            # create a new face with the surface
            brep = BRepBuilderAPI_MakeFace(occ_face, self.tolerance).Face()
            brep_face = BRep_Tool.Surface(brep)

            # cycle on the edges
            edge_explorer = TopExp_Explorer(nurbs_face, TopAbs_EDGE)
            while edge_explorer.More():
                edge = topods_Edge(edge_explorer.Current())
                # edge in the (u,v) coordinates
                edge_uv_coordinates = BRep_Tool.CurveOnSurface(edge, face_aux)
                # evaluating the new edge: same (u,v) coordinates, but
                # different (x,y,x) ones
                edge_phis_coordinates_aux = BRepBuilderAPI_MakeEdge(
                    edge_uv_coordinates[0], brep_face
                )
                edge_phis_coordinates = edge_phis_coordinates_aux.Edge()
                tol.SetTolerance(edge_phis_coordinates, self.tolerance)
                wire_maker.Add(edge_phis_coordinates)
                edge_explorer.Next()

            # grouping the edges in a wire, this fails if there are is more than one edge (for example, a circle with
            # a hole).
            # TODO: separate the edges into connected sets and create separate wires.
            # Let the user select faces for deletion or create more complicated topology by repeateldly adding
            wire = None
            try:
                wire = wire_maker.Wire()
            except:
                print(
                    "creating a wire failed, this is likely because the surface has a hole. the excess face can be "
                    "removed from the result"
                )
                # create a new wire with any edge, this doesn't have an effect on the face, just to satisfy the method
                # definition
                wire_maker = BRepBuilderAPI_MakeWire()
                wire_maker.Add(edge)
                wire = wire_maker.Wire()

            # trim the surface with the wire
            brep_surf = BRepBuilderAPI_MakeFace(occ_face, wire).Shape()

            # add the new face to the shape
            compound_builder.Add(compound, brep_surf)
            n_faces += 1
            faces_explorer.Next()
        self.write_shape_to_file(compound, self.outfile)

    @staticmethod
    def get_normal_for_point(face, u, v):
        try:
            return GeomLProp_SLProps(face, u, v, 1, 0.01).Normal()
        except:
            print("normal undefined - calculating from nearby points")
            for i in range(0, 360, 10):
                try:
                    new_u = u + 0.001 * math.sin(i)
                    new_v = v + 0.001 * math.cos(i)
                    return GeomLProp_SLProps(face, new_u, new_v, 1, 0.01).Normal()
                except:
                    print("could not calculate normal")
                    pass

    def write_shape_to_file(self, shape, filename):
        """
        Abstract method to write the 'shape' to the `filename`.

        Not implemented, it has to be implemented in subclasses.
        """
        raise NotImplementedError(
            "Subclass must implement abstract method "
            + self.__class__.__name__
            + ".write_shape_to_file"
        )

    def show(self, show_file=None, points=None, deviation=None, mesh=None):
        """
        Method to show a file. If `show_file` is not given it plots
        `self.shape`.

        :param string show_file: the filename you want to show.
        """
        if show_file is None:
            shape = self.shape
        else:
            shape = self.load_shape_from_file(show_file)

        display, start_display, __, __ = init_display()
        display.FitAll()

        # visualize vectors
        if points is not None and deviation is not None:
            wires = []
            vertices = []
            for i in range(0, points.shape[0]):
                point = points[i]
                direction = deviation[i]
                v1 = gp_Pnt(point[0], point[1], point[2])
                v2 = gp_Pnt(
                    point[0] + direction[0],
                    point[1] + direction[1],
                    point[2] + direction[2],
                )
                try:
                    # Create an edge to visualization the deviation
                    edge = BRepBuilderAPI_MakeEdge(v1, v2).Edge()
                    wires.append(BRepBuilderAPI_MakeWire(edge).Wire())

                    # Create a vertex to visualize the control point
                    vertices.append(make_vertex(gp_Pnt(point[0], point[1], point[2])))
                except:
                    pass

            display.DisplayShape(wires, color="blue")
            display.DisplayShape(vertices, color="red")

        object = display.DisplayShape(shape, update=True)[0]

        if points is not None and deviation is not None:
            display.Context.SetTransparency(object, 0.8, True)

        if mesh is not None:
            stl_shape = read_stl_file(mesh)
            object = display.DisplayShape(stl_shape, update=True)[0]
            display.Context.SetTransparency(object, 0.5, True)

        # Show the plot to the screen
        start_display()
