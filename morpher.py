import sys
import numpy as np

from pathlib import Path

import argparse
from argparse import RawTextHelpFormatter
from Compensation.Algorithms import (
    calculate_closest_point_deviation,
    calculate_reverse_normal_deviation,
    apply_fix,
)


def check_file(filename, msg_not_specified, msg_not_found):
    if filename is None:
        print(msg_not_specified)
        sys.exit(1)
    if not Path(filename).is_file():
        print("{}: '{}'".format(msg_not_found, filename))
        sys.exit(1)


def read_numpy_array(filename, format):
    if format == "numpy":
        return np.load(filename)
    if format == "txt":
        return np.loadtxt(filename)

    print("format '{}' is unknown".format(format))
    sys.exit(1)


def write_numpy_array(arr, filename, format):
    if format == "numpy":
        return np.save(filename, arr)
    if format == "txt":
        return np.savetxt(filename, arr)

    print("format '{}' is unknown".format(format))
    sys.exit(1)


def handle_show(args):
    check_file(args.cad_in, "input cad not specified", "input cad not found")

    points = None
    normals = None
    if args.control_points_in is not None:
        points = read_numpy_array(args.control_points_in, args.control_points_format)
    if args.normals_in is not None:
        normals = read_numpy_array(args.normals_in, args.control_points_format)

    from PyGemWrapper.IgesHandler import IgesHandler

    IgesHandler().show(args.cad_in, points, normals, None)


def handle_points(args):
    check_file(args.cad_in, "input cad not specified", "input cad not found")

    from PyGemWrapper.IgesHandler import IgesHandler

    control_points, normals = IgesHandler().parse(args.cad_in)

    if args.control_points_out is not None:
        print("writing control points to: '{}'".format(args.control_points_out))
        write_numpy_array(
            control_points, args.control_points_out, args.control_points_format
        )
    if args.normals_out is not None:
        print("writing normals to: '{}'".format(args.normals_out))
        write_numpy_array(normals, args.normals_out, args.control_points_format)


def handle_morph(args):
    check_file(args.cad_in, "input cad not specified", "input cad not found")
    if args.cad_out is None:
        print("output path not specified")
        sys.exit(1)

    check_file(
        args.control_points_in,
        "control points not specified",
        "control points file not found",
    )

    from PyGemWrapper.IgesHandler import IgesHandler

    # read the cad file
    iges_handler = IgesHandler()
    iges_handler.parse(args.cad_in)

    # morph with the control points
    control_points = read_numpy_array(
        args.control_points_in, args.control_points_format
    )
    print("writing result to {}".format(args.cad_out))
    iges_handler.write(
        control_points, args.cad_out, 1e-6
    )  # TODO: Evaluate if needs to be a parameter


def handle_compensation(args):
    check_file(args.cad_in, "input cad not specified", "input cad not found")
    if args.cad_out is None:
        print("output path not specified")
        sys.exit(1)

    check_file(
        args.reference, "reference file not specified", "reference file not found"
    )

    if args.compensation_algorithm not in ("reverse-normals", "closest-point"):
        print(
            "only `reverse-normals` and `closest-point` compensation algorithm is supported"
        )

    from PyGemWrapper.IgesHandler import IgesHandler
    from Stl.StlHandler import StlHandler

    iges_handler = IgesHandler()
    control_points, normals = iges_handler.parse(args.cad_in)
    mesh = StlHandler()
    mesh.load(args.reference)

    # perform the compensation
    if args.compensation_algorithm == "closest-point":
        deviation = calculate_closest_point_deviation(mesh, control_points)
    elif args.compensation_algorithm == "reverse-normals":
        deviation = calculate_reverse_normal_deviation(
            mesh, control_points, normals.copy()
        )
    else:
        raise Exception("Unknown compensation algorithm")

    new_control_points = control_points.copy()
    apply_fix(new_control_points, deviation)

    # visualize the geometry with the changes to control points
    iges_handler.parse(args.cad_in)
    iges_handler.show(None, control_points, deviation, args.reference)

    print("writing result to {}".format(args.cad_out))
    iges_handler.write(
        new_control_points, args.cad_out, 1e-6
    )  # TODO: Evaluate if needs to be a parameter

    # visualize the geometry with the changes to control points
    iges_handler.show(args.cad_out)


def handle_deviation(args):
    check_file(args.cad_in, "input cad not specified", "input cad not found")
    check_file(
        args.reference_points_in,
        "input reference points not specified",
        "reference points not found",
    )

    check_file(
        args.reference, "reference file not specified", "reference file not found"
    )

    if args.compensation_algorithm not in ("reverse-normals", "closest-point"):
        print(
            "only `reverse-normals` and `closest-point` compensation algorithm is supported"
        )

    from PyGemWrapper.IgesHandler import IgesHandler
    from Stl.StlHandler import StlHandler

    reference_points = read_numpy_array(
        args.reference_points_in, args.control_points_format
    )

    # calculate deviation
    mesh = StlHandler()
    mesh.load(args.reference)
    if args.compensation_algorithm == "closest-point":
        deviation = calculate_closest_point_deviation(mesh, reference_points)
    elif args.compensation_algorithm == "reverse-normals":
        check_file(args.normals_in, "input normals not specified", "normals not found")
        normals = read_numpy_array(args.normals_in, args.control_points_format)
        deviation = calculate_reverse_normal_deviation(mesh, reference_points, normals)
    else:
        raise Exception("Unknown compensation algorithm")

    # visualize the geometry with the deviations
    iges_handler = IgesHandler()
    iges_handler.parse(args.cad_in)
    iges_handler.show(None, reference_points, deviation, args.reference)

    if args.deviation_out is not None:
        print("writing result to {}".format(args.deviation_out))
        write_numpy_array(deviation, args.deviation_out, args.control_points_format)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Uses PyGem to manipulate CAD models in IGES format. Optionally output the control points "
        "to be updated by another program and used to manipulate the CAD model. Optionally use a "
        "reference STL file to calculate deviation from a measurement and perform compensation along "
        "normals",
        formatter_class=RawTextHelpFormatter,
    )

    parser.add_argument(
        "action",
        type=str,
        help="SHOW           visualize the input cad model, optionally with control point normals\n"
        "POINTS         output the models control points to file\n"
        "MORPH          morph the model using input control points\n"
        "DEVIATION      calculate deviation from reference data\n"
        "COMPENSATION   morph the model using a reference STL file",
    )

    parser.add_argument(
        "-i", "--cad-in", type=str, help="original CAD model in IGES format"
    )
    parser.add_argument(
        "-o",
        "--cad-out",
        type=str,
        help="output morphed CAD model to file (must end with .igs/.iges",
    )
    parser.add_argument(
        "-r", "--reference", type=str, help="reference file in STL format"
    )

    parser.add_argument(
        "-c",
        "--compensation-algorithm",
        type=str,
        help="compensation algorithm (`reverse-normals` / `closest-point`)",
        default="closest-point",
    )

    parser.add_argument(
        "-ci", "--control-points-in", type=str, help="use these control points"
    )
    parser.add_argument("-ni", "--normals-in", type=str, help="use these normals")
    parser.add_argument(
        "-co",
        "--control-points-out",
        type=str,
        help="output control points to file (no file ending)",
    )
    parser.add_argument(
        "-no",
        "--normals-out",
        type=str,
        help="output normals control points to file (no file ending)",
    )
    parser.add_argument(
        "-ri",
        "--reference-points-in",
        type=str,
        help="calcualte deviation for these points",
    )
    parser.add_argument(
        "-do",
        "--deviation-out",
        type=str,
        help="write deviation to file (no file ending)",
    )
    parser.add_argument(
        "-f",
        "--control-points-format",
        type=str,
        help="format for control points (numpy/txt)",
        default="numpy",
    )

    args = parser.parse_args()
    print(args)

    # only output control points
    if args.action == "SHOW":
        handle_show(args)
    if args.action == "POINTS":
        handle_points(args)
    if args.action == "MORPH":
        handle_morph(args)
    if args.action == "DEVIATION":
        handle_deviation(args)
    if args.action == "COMPENSATION":
        handle_compensation(args)
